import time

import redis
import os 
from flask import Flask

app = Flask(__name__)
redis_host=os.getenv('REDIS_HOST', 'redis')
cache = redis.Redis(host=redis_host, port=6379)

def get_hit_count():
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)

def check_redis():
    try:
        return cache.ping()
    except redis.exceptions.ConnectionError as exc:
        raise exc

@app.route('/')
def hello():
    count = get_hit_count()
    hostname = os.getenv('HOSTNAME')
    return 'Hello World from {}! I have been seen {} times.\n'.format(hostname,count)

@app.route('/liveness')
def check_liveness():
    hostname = os.getenv('HOSTNAME')
    return "I'm ok, I'm running from {}!\n".format(hostname)

@app.route('/readiness')
def check_readiness():
    test_redis  = check_redis()
    hostname = os.getenv('HOSTNAME')
    return "I'm ok, I'm running from {} and I'm connected to redis.\n".format(hostname)
